from __future__ import absolute_import, division, print_function, unicode_literals

from django.core.validators import EMPTY_VALUES
from django.db import models
from django.utils.translation import ugettext_lazy as _

from . import forms, utils, validators


class ListFieldMixin(object):
    """
    A model field that can hold a list of token-separated items. Values are
    stored as a string, but that data is provided as a Python list.

    Items in the list can be validated by subclassing
    """
    default_strict = True
    default_token = ','
    default_default = list
    item_name = _('item')
    item_name_plural = _('items')

    default_validators = [validators.validate_list]
    description = _('List of items')

    def __init__(self, *args, **kwargs):
        # Pop these from kwargs as they are custom.
        self.strict = kwargs.pop('strict', self.default_strict)
        self.token = kwargs.pop('token', self.default_token)

        # But keep this in kwargs as it pertains to the superclass. (We set
        # default as a convenience.)
        defaults = {'default': self.default_default}
        defaults.update(kwargs)

        super(ListFieldMixin, self).__init__(*args, **defaults)

    def formfield(self, **kwargs):
        defaults = {
            'form_class': forms.ListField,
            'token': self.token,
            'error_messages': {
                'invalid': (_('Enter only %(items)s separated by "%(token)s".') % 
                            {'items': self.item_name_plural, 'token': self.token}),
            },
        }
        defaults.update(kwargs)

        return super(ListFieldMixin, self).formfield(**defaults)

    def deconstruct(self):
        name, path, args, kwargs = super(ListFieldMixin, self).deconstruct()

        # Only include kwarg if it's not the default
        if self.strict != self.default_strict:
            kwargs['strict'] = self.strict
        if self.token != self.default_token:
            kwargs['token'] = self.token
        if self.default != self.default_default:
            kwargs['default'] = self.default

        return name, path, args, kwargs

    def _listify(self, value):
        if value is None:
            return None
        elif value in EMPTY_VALUES:
            return []
        value = self.parse_list(value)

        # It is not normal to run validators at this point, but we do by default
        # to prevent loading a list containing invalid objects.
        if self.strict:
            self.run_validators(value)

        return value

    def to_python(self, value):
        return self._listify(value)

    def from_db_value(self, value, *args, **kwargs):
        return self._listify(value)

    def _stringify(self, value):
        # It is not normal to run validators at this point, but we do by default
        # to prevent saving a list containing invalid objects.
        if self.strict:
            self.run_validators(value)

        return self.unparse_list(value)

    def value_to_string(self, obj):
        value = self._get_val_from_obj(obj)
        if value is None:
            return self.get_default()
        return self._stringify(value)

    def get_prep_value(self, value):
        # -- Pre-1.9 TextField bug workaround --
        value = models.Field.get_prep_value(self, value)
        value = self.to_python(value)
        # Ideally we could have just done the following:
        #     value = super(ListFieldMixin, self).get_prep_value(value)
        # But there is a bug with TextField that is not fixed until Django 1.9
        # - https://code.djangoproject.com/ticket/24677
        # - https://code.djangoproject.com/changeset/19e67c6
        # To support mixing in with both CharField and TextField on Django 1.8
        # we need this odd construction.
        # -- END work around --

        if value is None:
            return value
        return self._stringify(value)

    def parse_list(self, value):
        """
        This is the method to override if you need to change how the string is
        turned into a list.

        The ``value`` kwarg will not be None or empty, and is (most likely) a
        string of some type.
        """
        return utils.parse_list(value, self.token)

    def unparse_list(self, value):
        """
        This is the method to override if you need to change how the list is
        turned into a string.

        The ``value`` kwarg will not be None, and is (most likely) a list.
        """
        return self.token.join(value)


class ListCharField(ListFieldMixin, models.CharField):
    pass


class ListTextField(ListFieldMixin, models.TextField):
    pass


class EmailListFieldMixin(object):
    """ A model field that can hold a list of email addresses. """
    item_name = _('email address')
    item_name_plural = _('email addresses')

    default_validators = [validators.validate_email_list]
    description = _('List of email addresses')

    def formfield(self, **kwargs):
        defaults = {'validators': self.validators}
        defaults.update(kwargs)
        return super(EmailListFieldMixin, self).formfield(**defaults)


class EmailListCharField(EmailListFieldMixin, ListCharField):
    pass


# Keep this import around for backwards compatibility with migrations prior to
# the mixin refactor.
EmailListField = EmailListCharField


class EmailListTextField(EmailListFieldMixin, ListTextField):
    pass
