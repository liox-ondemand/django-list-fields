from __future__ import absolute_import, division, print_function, unicode_literals

from django.core.exceptions import ValidationError
from django.utils import six
from django.utils.translation import ugettext_lazy as _


def parse_list(value, token=','):
    """
    Accepts a string containg zero or more items separated by 'token', or an
    iterable containing zero or more items. A list of unique items is returned.
    """
    if isinstance(value, six.string_types):
        value = value.split(token)

    try:
        iter(value)
    except TypeError:
        raise ValidationError(_('Invalid input for email list'), code='invalid')

    # Create unique list that omits any empty string items.
    unique_list = []

    for item in value:
        bit = item.strip()
        if bit and bit not in unique_list:
            unique_list.append(bit)

    return unique_list
