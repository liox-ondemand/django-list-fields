from __future__ import absolute_import, division, print_function, unicode_literals

from unittest import TestCase

from django.core.exceptions import ValidationError

from . import utils


class ParseList(TestCase):
    """ Exercise the utils.parse_list function """

    def test_empty_str(self):
        self.assertListEqual(utils.parse_list(''), [])

    def test_empty_only_sep_str(self):
        self.assertListEqual(utils.parse_list(',,'), [])

    def test_empty_iterable(self):
        self.assertListEqual(utils.parse_list([]), [])

    def test_non_iterable(self):
        with self.assertRaises(ValidationError):
            utils.parse_list(object)

    def test_items(self):
        self.assertListEqual(utils.parse_list('e,o,cy'), ['e', 'o', 'cy'])

    def test_non_default_sep(self):
        self.assertListEqual(utils.parse_list('a/b', token='/'), ['a', 'b'])
