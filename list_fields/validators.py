from __future__ import absolute_import, division, print_function, unicode_literals

from django.core import validators 
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


def noop_validator(value):
    """ A simple place-holder to make base class logic reusable. """
    pass


def validate_list(value, validate_item=noop_validator):
    """
    Calls 'validator' for each item in 'value'. This is essentially a no-op
    that holds reusable validation and error message logic.
    """
    errors = []

    for item in value:
        try:
            validate_item(item)
        except ValidationError as error:
            errors.append(error)

    if errors:
        raise ValidationError(errors)


def validate_email(value):
    """
    A simple wrapper so that we can customize the error message to say *which*
    value is invalid. This is necessary because we use this validator on a list
    of items.
    """
    try:
        validators.validate_email(value)
    except ValidationError as error:
        raise ValidationError(_('Invalid email address: %(value)s'),
                              code='item_invalid', params={'value': value})


def validate_email_list(value, validator=validate_email):
    """ Validates that each item in a list is a valid email address. """
    validate_list(value, validator)
