from __future__ import absolute_import, division, print_function, unicode_literals

from django import forms
from django.core.validators import EMPTY_VALUES
from django.utils import six

from . import utils, validators


class ListField(forms.CharField):
    """
    A form field that can hold a list of items.
    
    The field allows the user to enter token-separated strings but provides
    that data as a Python list.

    List and item validation can be customized by passing a 'validators'
    kwarg when creating the field, just like usual.
    """
    default_token = ','
    default_validators = [validators.validate_list]

    def __init__(self, *args, **kwargs):
        self.token = kwargs.pop('token', self.default_token)
        super(ListField, self).__init__(*args, **kwargs)

    def prepare_value(self, value):
        if isinstance(value, six.string_types):
            # Don't validate this because if they entered an invalid string we
            # need to return it as-is.
            return value
        return self.token.join(value)
    
    def to_python(self, value):
        if value is None:
            return None
        elif value in EMPTY_VALUES:
            return []
        return utils.parse_list(value, self.token)
