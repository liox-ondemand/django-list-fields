====================
 Django List Fields
====================

.. warning:: This is still in the very early stages of development. Anything
             within the package may change at any time.

If you are fortunate enough to be on Django 1.8 yet unfortunate enough to be
using something other than Postgres (I’m looking at you, `array fields`_), you
may find this package useful. This package provides a nice list-ish interface
to token-separated char-based model and form field data.

.. _array fields: https://docs.djangoproject.com/en/1.8/ref/contrib/postgres/

Currently only Django 1.8 is supported. Contributions that add support to 1.7
are welcome.

Quick Example
-------------
Given this model::

    class Foo(models.Model):
        emails = list_fields.models.EmailListField()

You can do the following::

    >>> foo = Foo()
    >>> foo.emails
    []
    >>> foo.emails.append('me@here.com')
    >>> foo.emails
    ['me@here.com']
    >>> foo.save()
    >>> foo.emails.append('invalid-email')
    >>> foo.save()
    ValidationError ...

Yet in the admin and form fields users just enter token-separated strings,
such as ``me@here.com,you@there.com``.

Documentation
-------------

Current two model fields are provided:

``ListField``
    A token-separated list field. No validation of list items is performed.

``EmailListField``
    A subclass of ``ListField`` that validates that each item is a valid
    email address.

There is ``ListField`` form field as well, which is used by the above model
fields. As exemplified by the ``EmailListField`` model field, if you want a
custom model ``ListField`` subclass, you can create that without needing to also
create a custom form field; simply override your model ``ListField`` subclass'
``formfield`` method and pass the validators to use.
