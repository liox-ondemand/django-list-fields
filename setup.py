import os
from setuptools import setup


here = os.path.dirname(__file__)


def get_long_desc():
    with open(os.path.join(here, 'README.rst')) as readme:
        return readme.read()


# Function borrowed from carljm.
def get_version():
    with open(os.path.join(here, 'list_fields', '__init__.py')) as fh:
        for line in fh.readlines():
            if line.startswith('__version__ ='):
                return line.split('=')[1].strip().strip("'")


setup(
    name='django-list-fields',
    version=get_version(),
    description='Django model and form fields for handling token-separated lists',
    url='https://bitbucket.org/liox-ondemand/django-list-fields/',
    author='Lionbridge Technologies, Inc.',
    author_email='ondemand@lionbridge.com',
    license='BSD',
    download_url='https://bitbucket.org/liox-ondemand/django-list-fields/get/v{0}.zip'.format(get_version()),
    long_description=get_long_desc(),
    install_requires=[
        'Django>=1.8,<2.0',
    ],
    packages=[
        'list_fields',
    ],
    package_data={
        'list_fields': [
            'locale/*/LC_MESSAGES/*',
        ],
    },
    classifiers=[
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django :: 1.8',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Topic :: Internet :: WWW/HTTP :: Site Management',
    ],
)
